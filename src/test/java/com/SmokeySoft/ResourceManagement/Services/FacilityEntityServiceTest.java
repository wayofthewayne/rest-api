package com.SmokeySoft.ResourceManagement.Services;

import com.SmokeySoft.ResourceManagement.Models.Facility;
import com.SmokeySoft.ResourceManagement.Models.FacilityEntity;
import com.SmokeySoft.ResourceManagement.Repository.FacilityRepo;


import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.*;

@SpringBootTest
@ActiveProfiles("test")
public class FacilityEntityServiceTest {

    @MockBean
    FacilityRepo facilityMockRepository;

    @Autowired
    FacilityService facilityService;

    @Autowired
    ModelMapper modelMapper;

    //Test to see if facility saves
    @Test
    public void testSaveFacility(){

        Facility facilityToBeSaved = new Facility( "Office Space",3,100.5,25.3, "Board");
        //Storing details of the facility that is to be saved

        Facility expectedFacilityToBeSaved = new Facility(1L, "Office Space", 3, 100.5, 25.3, "Board");
        //We expect the saved facility to have this structure

        //We create the entity we wish to mock
        FacilityEntity outputEntity = new FacilityEntity(1L, "Office Space", 3, 100.5, 25.3, "Board");

        //mocking the facility repository for save under any facility entity
        when(facilityMockRepository.save(any(FacilityEntity.class))).thenReturn(outputEntity);
        Facility savedFacility = facilityService.saveFacility(facilityToBeSaved);
        //saving the facility through the service layer

        assertThat(savedFacility).isEqualToComparingFieldByField(expectedFacilityToBeSaved);
        //comparing field by field, making sure the saved facility is as expect

        verify(facilityMockRepository, times(1)).save(any(FacilityEntity.class));
        //verifying we hit the repository at least once.

    }

    //Test to find  a facility by Id
    @Test
    public void testFindExistingFacilityByID(){
        Long id = 1L;
        Facility facilityToBeReturned = new Facility( id,"Office Space", 3, 100, 5, "Whiteboard");
        //Creating the facility which we are expecting to be returned

        FacilityEntity outputEntity = new FacilityEntity(id,"Office Space", 3, 100, 5, "Whiteboard");
        //we shall mock this

        Optional<FacilityEntity> outputEntityOptional = Optional.of(outputEntity);
         //mapping the entity to an optional one, since this is what the function returns in the jpa repo documentation

        when(facilityMockRepository.findById(id)).thenReturn(outputEntityOptional); //performing the mock
        Facility facilityReturned = facilityService.getFacilityById(facilityToBeReturned.getId());
        //retreiving the returned value

        verify(facilityMockRepository, times(1)).findById(id);
        //making sure the function was called

        assertThat(facilityReturned).isEqualToComparingFieldByField(facilityToBeReturned);
        //enforcing that the values returned are the ones we expected

    }

    //Test to try find a facility which is not in the system
    @Test
    public void testFindingNonexistentFacilityByID(){
        Facility doesNotExist = new Facility(3L, null, 0, 0.0, 0, null);
        //we create a facility which we expect not to exist in the database

        Facility facRet = facilityService.getFacilityById(doesNotExist.getId());
        //we perform the function call

        assertThat(facRet).isEqualTo(null); //make sure that null has been returned
        //By function definition this is what we expect

        verify(facilityMockRepository, times(1)).findById(3L);
        //verify that a search was attempted on this id.
    }

    //Test to try and find all facilities
    @Test
    public void testFindAllFacility(){
        List <Facility> outputFacilities = new ArrayList<Facility>(); //list to hold the expected return values

        Facility fac1 = new Facility(1L, "Office Space", 2, 150, 25,"Interactive Board");
        Facility fac2 = new Facility(2L, "Board Room", 5, 300, 56,"Fast Wifi");
        outputFacilities.add(fac1);
        outputFacilities.add(fac2); //adding them to the list

        List<FacilityEntity> outputEntities = new ArrayList<FacilityEntity>(); //we prepare the mock for jpa function

        FacilityEntity ent1 = new FacilityEntity(1L, "Office Space", 2, 150, 25,"Interactive Board");
        FacilityEntity ent2 = new FacilityEntity(2L, "Board Room", 5, 300, 56,"Fast Wifi");
        outputEntities.add(ent1);
        outputEntities.add(ent2);

        when(facilityMockRepository.findAll()).thenReturn(outputEntities); //setting the mock
        List<Facility> foundFacilities = facilityService.getAllFacilities();
        //performing function call through the service layer

        verify(facilityMockRepository, times(1)).findAll();
        //verifying the function call of the jpa repo was called once

        for(int i=0; i<outputFacilities.size(); i++){
            assertThat(foundFacilities.get(i)).isEqualToComparingFieldByField(outputFacilities.get(i));
        }//making sure each facilities in the list, matches that as we expect for each of their individual fields.
    }

    //test to find all the facilities when there are none
    @Test
    public void findAllFacilityEmpty(){
        List<Facility> foundFacilities = facilityService.getAllFacilities();
        //performing fn call through service layer
        //in this case there is no need to mock

        assertNull(foundFacilities); //we expect null to returned to be returned.

        verify(facilityMockRepository, times(1)).findAll();
        //verifying we attempted a search
    }

    //Function test the delete functionality
    @Test
    public void testDeleteFacility(){

        FacilityEntity entityToBeDeleted = new FacilityEntity(1L,"Office Space", 3, 100, 5, "Music System");

        Optional<FacilityEntity> optionalEntity = Optional.of(entityToBeDeleted);
        //mapping the entity to an optional one, since this is what the function returns in the jpa repo documentation

        when(facilityMockRepository.findById(1L)).thenReturn(optionalEntity);
        //mocking the search, since the function will first call this

        boolean retVal = facilityService.deleteFacilityById(1L); //calling the delete function in the service layer

        verify(facilityMockRepository, times(1)).deleteById(1L);
        //making sure the repository called the delete function under the id, for 1 time

        assertTrue(retVal); //asserting that the return value was true.
    }

    //test to try and delete a facility which does not exist
    @Test
    public void testDeleteNonExistentFacility(){
        boolean retVal = facilityService.deleteFacilityById(1L);
        assertFalse(retVal);
        //Calling the delete on a no-existent facility, making sure it returns false.

        verify(facilityMockRepository, times(1)).findById(1L);
        //verifying we actually attempted a search
    }

    //Test to update a facility
    @Test
    public void testUpdateAFacility(){

        Long id = 1L; //This is the id we wish to pass

        Facility expectedReturn = new Facility(id,"Hot Desk", 3, 1000, 350,"Coffee Machine" );
        //This is how we want the facility to be updated as. Therefore it is also the expected return

        FacilityEntity mockEntity = new FacilityEntity(id, "Hot Desk", 3, 1000, 350,"Coffee Machine");
        //we create a mock entity to be saved

        //Creating the entity already in the system to be mocked
        FacilityEntity beforeUpdate = new FacilityEntity(id, "Office Space",5, 100,5, "Nothing" );
        Optional<FacilityEntity> foundInSystem = Optional.of(beforeUpdate);

        when(facilityMockRepository.findById(id)).thenReturn(foundInSystem); //we mock the exists and save function of the repository
        when(facilityMockRepository.save(any(FacilityEntity.class))).thenReturn(mockEntity);

        Facility returnedFacility = facilityService.updateFacility(id, expectedReturn);
        //we perform the function call through the service layer with the appropriate parameters

        //We make sure, exist and save functions were invoked once each.
        verify(facilityMockRepository, times(1)).findById(id);
        verify(facilityMockRepository, times(1)).save(any(FacilityEntity.class));

        assertThat(returnedFacility).isEqualToComparingFieldByField(expectedReturn);
        //We also make sure the returned value is as we expected.
        //Note this way we made the sure the same Id that was deleted was replaced
    }

    //Test to try an update a facility not in the system
    @Test
    public void testEmptyUpdate(){
        Long id =1L;
        Facility facilityToTryDelete = new Facility(id, "Hot Desk", 4, 995, 345, "Fast Wifi");
        //This is the facility we expect to not exist

        Facility returnedFac = facilityService.updateFacility(id , facilityToTryDelete);
        //we call the function

        assertNull(returnedFac); //make sure it returns null
    }
}
