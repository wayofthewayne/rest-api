package com.SmokeySoft.ResourceManagement.Models;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
public class FactoryPatternDemo {


    //This test servers to demo the factory design pattern implemented
    //Here we showcase how a model can be created and manipulated
    @Test
    public void FactoryPatternDemoTest(){

        ProductFactory factory = new ProductFactory(); //We create an instance of a factory
        //This is responsible for building any model


        //below we will see all the ways a product can be created
        //Note this allows the client to not worry about the different Models as he can simply work using the product
        Product facility = factory.getFacilityModel("normal", 5L, "Hot Desk", 15,250,125, "chairs,computers");
        Product facilityRequest = factory.getFacilityModel("request", 0L, "Hot Desk", 5,100,55, "laptop");
        Product facilityEntity = factory.getFacilityModel("entity", 2L, "Office Space", 15,1000,125, "Sound System");
        Product facilityResponse = factory.getFacilityModel("response", 3L, "Hot Desk", 16,2000,400, "nothing");

        //Setting and retrieving attributes of model through product (for attributes common to all models)
        facility.setEquipment("Magic wand");
        System.out.println(facility.getEquipment());

        //We can easily type cast to the model and use it's functions
        FacilityResponse facilityResponse1 = (FacilityResponse) facilityResponse;
        facilityResponse1.FacilityDisplay();

        //more type casting
        FacilityEntity facilityEntity1 = (FacilityEntity)  facilityEntity;
        FacilityRequest facilityRequest1 = (FacilityRequest) facilityRequest;

        //Assertion to show typecasting works as intended
        assertEquals(facilityEntity1.getClass(), FacilityEntity.class);
        assertEquals(facilityRequest1.getClass(), FacilityRequest.class);
    }
}
