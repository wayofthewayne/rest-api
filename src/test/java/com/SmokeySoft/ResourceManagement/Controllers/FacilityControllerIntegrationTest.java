package com.SmokeySoft.ResourceManagement.Controllers;

import com.SmokeySoft.ResourceManagement.Models.*;
import com.SmokeySoft.ResourceManagement.Services.FacilityService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class FacilityControllerIntegrationTest {

    private static final ObjectMapper om = new ObjectMapper();


    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private FacilityService facilityMockService; //mocking the service

    //Test to see create functionality
    @Test
    public void testCreateValidFacility() throws Exception{

        FacilityRequest facilityRequest = new FacilityRequest("Office Space", 3, 100.5, 15.0, "Board");
        //generating a request for a facility

        FacilityResponse expectedFacilityResponse = new FacilityResponse(1L, "Office Space", 3, 100.5, 15.0, "Board");
        //the response which is expect to be returned

        //Changing types from response to string
        String expectedResponseBody = om.writeValueAsString(expectedFacilityResponse);

        //We create the mock facility, and set it when saveFacility is called
        Facility servicedFacility = new Facility(1L, "Office Space", 3, 100.5, 15.0, "Board");
        when(facilityMockService.saveFacility(any(Facility.class))).thenReturn(servicedFacility);

        String endpoint = "/facility";
        //Creating the endpoint for http REST communication

        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(endpoint, facilityRequest, String.class);
        //Will create a new facility passing along the response back using HTTP post method.
        //(url to post request, object to be posted, the type of response body)
        //calls the new facility method (through matching endpoints)

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        //Making sure we get the expected status code in this case

        JSONAssert.assertEquals(expectedResponseBody, responseEntity.getBody(), true);
        //asserting that the value of the entity (stored in the database) is the same as expected
    }

    //Below are several tests to check the constraints
    @Test
    public void testCreateNonExistentPrice(){
        FacilityRequest facilityRequest = new FacilityRequest("Board Room", 3, -1, 15.0, "Chalk board");
        //Create a request breaking the constraint

        String endpoint = "/facility";//map the endpoint

        //performing post for entity
        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(endpoint, facilityRequest, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        //enforcing expected status code
    }

    @Test //similar to above on maintenance constraint
    public void testCreateNonExistentMaintenance(){
        FacilityRequest facilityRequest = new FacilityRequest("Board Room", 3 , 100.02, -1, "5Chairs");

        String endpoint = "/facility";

        ResponseEntity<String > responseEntity = testRestTemplate.postForEntity(endpoint, facilityRequest, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test //similar to above on capacity constraint
    public void testCreateNonExistentCapacity(){
        FacilityRequest facilityRequest = new FacilityRequest("Board Room", 0, 100.05, 50, "Projector");

        String endpoint = "/facility";

        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(endpoint, facilityRequest, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test //similar to above on type constraint
    public void testLeaveTypeOut(){
        FacilityRequest facilityRequest = new FacilityRequest( "    ", 1,100,5,null);

        String endpoint = "/facility";

        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(endpoint, facilityRequest, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    //testing the finding by id functionality
    @Test
    public void testFindingAFacility() throws Exception{

        Long id = 1L; //id to be retrieved

        FacilityResponse expectedResponse = new FacilityResponse(1L, "Hot Desk", 2, 225.79, 50, "Projector,3Laptops");
        String expectedResponseBody = om.writeValueAsString(expectedResponse);
        //We declare the expected respone, and convert it to string (JSON)

        Facility servicedFacility = new Facility(id, "Hot Desk", 2, 225.79, 50, "Projector,3Laptops");
        when(facilityMockService.getFacilityById(id)).thenReturn(servicedFacility);
        //We mock the service functionality

        String endpoint = "/facility/"; //define the end point

        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(endpoint + id, String.class);
        //we make a get request, note how the id is added to the endpoint

        assertEquals(HttpStatus.FOUND, responseEntity.getStatusCode());
        //assert the code is expected

        JSONAssert.assertEquals(expectedResponseBody, responseEntity.getBody(), true);
        //assert the values are equal
    }

    //Testing to find a non existent facility
    @Test
    public void testFindNonExistentFacility() throws JSONException {
        Long id = 33L; //this is the ID we are trying to find, expecting it not to exist

        String endpoint = "/facility/"; //we define the endpoint

        //we attempt to fetch the entity
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(endpoint + id, String.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        //We make sure the status code is that of not found

        JSONAssert.assertEquals(null, responseEntity.getBody(), true);
        //we make sure the body is null. (i.e contains no data)
    }

    //test to find all the facilities
    @Test
    public void testReturnAllFacilities() throws JsonProcessingException, JSONException {

        FacilityResponse outputFac1 = new FacilityResponse(1L, "Office Room", 10, 5755, 500, "Projector");
        FacilityResponse outputFac2 = new FacilityResponse(2L, "Hot Desk", 12, 4500, 500, "ChalkBoard");

        List <FacilityResponse> expectedResponse = new ArrayList<>();
        expectedResponse.add(outputFac1);
        expectedResponse.add(outputFac2); //populating the expected response list

        String expectedResponseBody = om.writeValueAsString(expectedResponse);

        Facility mockFac1 = new Facility(1L, "Office Room", 10, 5755, 500, "Projector");
        Facility mockFac2 = new Facility(2L, "Hot Desk", 12, 4500, 500, "ChalkBoard");
        List<Facility> mockList = new ArrayList<>();
        mockList.add(mockFac1);
        mockList.add(mockFac2); //populating the mocking list

        when(facilityMockService.getAllFacilities()).thenReturn(mockList);
        //mocking the service facility

        String endpoint = "/facility/";//defining the endpoint for findAll

        //We want an array of responses to be returned
        ResponseEntity<FacilityResponse[]> responseEntity = testRestTemplate.getForEntity(endpoint, FacilityResponse[].class);
        FacilityResponse[] allret = responseEntity.getBody(); //retrieve their bodies
        String retVal = om.writeValueAsString(allret);//convert to string

        assertEquals(HttpStatus.FOUND,responseEntity.getStatusCode());
        //make sure the code is found

        JSONAssert.assertEquals(expectedResponseBody, retVal, true);
        //make sure all the values match
    }

    //Test to find all the facilities when non exist
    @Test
    public void testEmptyListOfFacilities(){
        String endpoint = "/facility/";//defining the endpoint for findAll

        //We want an array of responses to be returned
        when(facilityMockService.getAllFacilities()).thenReturn(null);
        ResponseEntity<FacilityResponse[]> responseEntity = testRestTemplate.getForEntity(endpoint, FacilityResponse[].class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNull(responseEntity.getBody());
        //Asserting that is was not found and the body of the returned entity is null
    }

    //Test to delete a facility
    @Test
    public void testDeleteFacility(){
        String endpoint = "/facility/"; //defining endpoint
        Long id = 1L; //defining the id to be deleted

        HttpEntity<Long> idRequest = new HttpEntity<Long>(id); //creating request entity

        when(facilityMockService.deleteFacilityById(id)).thenReturn(true); //mocking facility service delete
        ResponseEntity<FacilityResponse> returnedResponse = testRestTemplate.exchange(endpoint + id,HttpMethod.DELETE,idRequest,FacilityResponse.class );
        //Using HTTP method delete and return a response

        assertEquals(HttpStatus.NO_CONTENT, returnedResponse.getStatusCode());
        //assert expected response
    }

    //Test to delete a non existent facility
    @Test
    public void testDeleteOnNonExistentFacility(){
        String endpoint = "/facility/"; //endpoint definition
        Long id = 3L; //id which should not exist in system

        HttpEntity<Long> idRequest = new HttpEntity<>(id); //HTTP entity request

        when(facilityMockService.deleteFacilityById(id)).thenReturn(false); //return false when we attempt to find non-existent id
        ResponseEntity<FacilityResponse> returnedResponse = testRestTemplate.exchange(endpoint + id, HttpMethod.DELETE, idRequest, FacilityResponse.class);
        //performing delete request through test rest template.

        assertEquals(HttpStatus.BAD_REQUEST, returnedResponse.getStatusCode());
        //Making sure the returned status code is as expected
    }

    //test to update facility
    @Test
    public void testUpdateFacility() throws JsonProcessingException, JSONException {
        String endpoint = "/facility/"; //defining the endpoint
        Long id = 1L; //Defining the id

        Facility updateVersion = new Facility(id, "Hot Desk", 3, 58, 4.5, "Chairs,Boards,Whiteboards");
        //This is how the update facility is to look

        FacilityRequest request = new FacilityRequest("Hot Desk", 3, 58, 4.5, "Chairs,Boards,Whiteboards");
        HttpEntity<FacilityRequest> httpRequest = new HttpEntity<>(request);
        //We create a facility request and convert it to an HTTPEntity

        //We mock the service layer updateFacility function
        when(facilityMockService.updateFacility(any(Long.class),any(Facility.class))).thenReturn(updateVersion);
        ResponseEntity<FacilityResponse> returnedResponse = testRestTemplate.exchange(endpoint + id, HttpMethod.PUT, httpRequest, FacilityResponse.class);
        //We perform the put mapping, and retrieve the response

        //We converted the returned, as well as the updated version to JSON
        String convertToJsonExpected = om.writeValueAsString(updateVersion);
        String convertToJsonReturned = om.writeValueAsString(returnedResponse);

        //We compare the bodies and status codes
        assertEquals(HttpStatus.OK, returnedResponse.getStatusCode());
        JSONAssert.assertEquals(convertToJsonExpected, convertToJsonExpected, true);


    }

    //Test to update a facility which is not in the system
    @Test
    public void testUpdateForFakeFacility(){

        String endpoint = "/facility/";//Defining endpoint and id
        Long id = 1L;

        FacilityRequest request = new FacilityRequest("Hot Desk", 3, 58, 4.5, "Chairs,Boards,Whiteboards");
        //we create the request and turn it to an HTTP Entity
        HttpEntity<FacilityRequest> httpRequest = new HttpEntity<>(request);

        //We perform the put mapping
        ResponseEntity<FacilityResponse> returnedResponse = testRestTemplate.exchange(endpoint + id, HttpMethod.PUT, httpRequest, FacilityResponse.class);

        //We make sure null is returned, and the NOT_FOUND status code
        assertNull(returnedResponse.getBody());
        assertEquals(HttpStatus.NOT_FOUND, returnedResponse.getStatusCode());
    }

}
