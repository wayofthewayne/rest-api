package com.SmokeySoft.ResourceManagement.Services;

import com.SmokeySoft.ResourceManagement.Models.FacilityEntity;
import com.SmokeySoft.ResourceManagement.Models.FacilityResponse;
import com.SmokeySoft.ResourceManagement.Repository.FacilityRepo;
import com.SmokeySoft.ResourceManagement.Utils.FacilityUtils;
import com.SmokeySoft.ResourceManagement.Models.Facility;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.rmi.ConnectIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


//below importations are required for communication
import com.SmokeySoft.Timetabling.*;
import org.springframework.web.client.RestTemplate;

@Service
public class ServiceImplementation implements FacilityService{

 @Autowired
    private FacilityRepo facilityRepo;

 @Autowired
    private FacilityUtils facilityUtils;

 @Autowired
    private ModelMapper modelMapper;

 @Override
 public Facility saveFacility(Facility facility){

     FacilityEntity facilityEntity = modelMapper.map(facility, FacilityEntity.class);
     //we convert the pass facility to an entity

     facilityEntity = facilityRepo.save(facilityEntity);
     //we store the entity in the repository

     Facility savedFacility = modelMapper.map(facilityEntity, Facility.class);
     //we need to return a facility so we map back
     return savedFacility;
 }

 @Override
 public List<Facility> getAllFacilities(){

     List<FacilityEntity> allFacilityEntities;
     //List to hold all the falicity entities

     allFacilityEntities =  facilityRepo.findAll();
     //fn call to retrieve all entities through the JPA repo

     if(allFacilityEntities.size() == 0){ //if the database is empty
         return null;
     }

     List<Facility> allFacilities = new ArrayList<Facility>();
     //List to hold all the facilities. These will be returned

     for(int i=0; i<allFacilityEntities.size(); i++){
         Facility toAddToList = modelMapper.map(allFacilityEntities.get(i), Facility.class);
         allFacilities.add(toAddToList);
         //Converting all entities to facilities and adding them to the list
     }
     return allFacilities;//returning the facility list
 }

 @Override
 public Facility getFacilityById(Long id){

      Optional<FacilityEntity> returnedFac; //jpa repo documentation requires optional

      returnedFac = facilityRepo.findById(id); //returning from the repository

     FacilityEntity returnedVal; //we want to remove the optional object, so we need an entity to store

      if(returnedFac.isPresent()) { //check if optional is null
           returnedVal = returnedFac.get(); //if not we map to an entity
      }else{
          System.out.println("Error retrieving. Please make sure u passed the right id");
          return null; //error message
      }

      Facility facilityToBeReturned = modelMapper.map(returnedVal, Facility.class);
      //mapping back to facility as we expect to return this

     return facilityToBeReturned;//returning the type
 }

 @Override
 public boolean deleteFacilityById(Long id){
     Facility tobeDeleted = getFacilityById(id);
     //locating the facility to be deleted

     if(tobeDeleted == null){//if not found return false
         return false;
     }

     facilityRepo.deleteById(id);
     //the entity is removed

     return true;
 }

 @Override
    public Facility updateFacility(Long id, Facility facility){

     Optional<FacilityEntity> toBeUpdated = facilityRepo.findById(id);
     //retrieve entity

     if(!toBeUpdated.isPresent()){ //if it does not exist, we return null
         return null;
     }

     FacilityEntity updatedVer = new FacilityEntity(); //Creating entity, and setting it's values
     updatedVer.setType(facility.getType());
     updatedVer.setCapacity(facility.getCapacity());
     updatedVer.setPrice(facility.getPrice());
     updatedVer.setMaintenance(facility.getMaintenance());
     updatedVer.setEquipment(facility.getEquipment());
     updatedVer.setId(id);


     updatedVer = facilityRepo.save(updatedVer);
     //Add it to the database

     Facility returnUpdated = modelMapper.map(updatedVer, Facility.class);
     //we map back to a facility and return it

     return returnUpdated;
 }

}
