package com.SmokeySoft.ResourceManagement.Services;

import com.SmokeySoft.ResourceManagement.Models.Facility;

import java.util.*;

//here we will hold all the methods which contain the business logic for the service layer for facility
public interface FacilityService {

    Facility saveFacility(Facility facility); //save function
    //can be used to update aswell ?


    List<Facility> getAllFacilities(); //read functions

    Facility getFacilityById(Long id);

    boolean deleteFacilityById(Long id);//delete Function

    Facility updateFacility(Long id,Facility facility);
}
