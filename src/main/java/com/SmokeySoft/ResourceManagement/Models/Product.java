package com.SmokeySoft.ResourceManagement.Models;


//Define an interface called Product
public interface Product {
    void setType(String type);
    void setPrice(double price);
    void setMaintenance(double maintenance);
    void setEquipment(String equipment);
    void setCapacity(int capacity);

    String getType();
    double getPrice();
    double getMaintenance();
    String getEquipment();
    int getCapacity();
}
