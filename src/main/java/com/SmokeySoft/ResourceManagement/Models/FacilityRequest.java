package com.SmokeySoft.ResourceManagement.Models;

import javax.validation.constraints.*;

public class FacilityRequest implements Product{

    //Constraints
    @NotBlank(message = "You must enter the type of facility")
    private String type;

    private String equipment;

    @Min(value =1, message = "minimum capacity is 1")
    private int capacity;

    @Min(value = 0,message = "minimum price is 0")
    private double price;

    @Min(value = 0, message = "minimum maintenance is 0")
    private double maintenance;

    public FacilityRequest(){ this(null,0,0.0,0,null); } //potentially useless method

    public FacilityRequest( String type, int capacity, double price, double maintenance, String equipment){
        this.setType(type);
        this.setCapacity(capacity);
        this.setPrice(price);
        this.setMaintenance(maintenance);
        this.setEquipment(equipment);
    }


    //getters and setters for capacity
    @Override
    public int getCapacity(){ return this.capacity; }
    @Override
    public void setCapacity(int capacity){ this.capacity = capacity; }

    //getters and setters for price
    @Override
    public double getPrice(){ return this.price; }
    @Override
    public void setPrice(double price){ this.price = price; }

    //getters and setters for maintenance
    @Override
    public double getMaintenance(){ return this.maintenance; }
    @Override
    public void setMaintenance(double maintenance){ this.maintenance = maintenance; }

    //getters and setters for Equipment
    @Override
    public String getEquipment(){ return this.equipment;}
    @Override
    public void setEquipment(String equipment){
        this.equipment = equipment;
    }

    //getters and setters for type
    @Override
    public String getType(){ return this.type; }
    @Override
    public void setType(String type){ this.type  = type; }



}
