package com.SmokeySoft.ResourceManagement.Models;

/*
* Can use building (to store all the facilities) in repository layer before storing in the dbms
* */

public class Facility implements Product{

    private Long facilityNo; //to hold the facility no (the unique identifier)

    private String type; //to hold the type of facility

    private String equipment; //will store the equipment
    private int capacity; //to hold the capacity
    private double price; //to hold the price
    private double maintenance; //to hold the maintenance cost

    public Facility(){ this(null, null, 0, 0.0, 0.0, null );}

    public Facility(String type, int capacity, double price, double maintenance, String equipment){
        this(null, null,capacity,price,maintenance,equipment);
    }
    //allows the creation of facility without any equipment (or id)
    //potentially change to allow equipment

    public Facility(Long id, String type, int capacity, double price, double maintenance,String equipment){
         this.setId(id);
         this.setCapacity(capacity);
         this.setPrice(price);
         this.setMaintenance(maintenance);
         this.setEquipment(equipment);
         this.setType(type);
    }

    //getters and setters for the ID
    public Long getId(){
        return this.facilityNo;
    }
    public void setId(Long id){
        this.facilityNo = id;
    }

    //getters and setters for capacity
    @Override
    public int getCapacity(){ return this.capacity; }
    @Override
    public void setCapacity(int capacity){ this.capacity = capacity; }

    //getters and setters for price
    @Override
    public double getPrice(){ return this.price; }
    @Override
    public void setPrice(double price){ this.price = price; }

    //getters and setters for maintenance
    @Override
    public double getMaintenance(){ return this.maintenance; }
    @Override
    public void setMaintenance(double maintenance){ this.maintenance = maintenance; }

    //getters and setters for Equipment
    @Override
    public String getEquipment(){ return this.equipment;}
    @Override
    public void setEquipment(String equipment){
           this.equipment = equipment;
    }

    //getters and setters for type
    @Override
    public String getType(){ return this.type; }
    @Override
    public void setType(String type){ this.type  = type; }

}
