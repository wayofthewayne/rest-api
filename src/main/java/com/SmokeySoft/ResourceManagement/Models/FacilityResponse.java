package com.SmokeySoft.ResourceManagement.Models;


public class FacilityResponse extends FacilityRequest{

    private Long facilityNo;

    public FacilityResponse(){ this(null,null,0,0.0,0,null); }
    public FacilityResponse(Long id, String type, int capacity, double price, double maintenance, String equipment){
        super(type,capacity,price,maintenance, equipment);
        this.setId(id);
    }

    //getters and setters for the ID
    public Long getId(){
        return this.facilityNo;
    }
    public void setId(Long id){
        this.facilityNo = id;
    }

    public void FacilityDisplay(){ //Display function
        System.out.println("Id: " + getId() + " Type: " + getType()
                + " Capacity: " + getCapacity() + " Price: " + getPrice()
                + " Maintenance: " + getMaintenance() + " Equipment: " + getEquipment() + "\n");
    }
}


