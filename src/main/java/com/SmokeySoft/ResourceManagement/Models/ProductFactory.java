package com.SmokeySoft.ResourceManagement.Models;


//This class may be used to allow the client to create the model he wishes, whilst abstracting himself from the creating process
public class ProductFactory {

    //based on the parameters passed, the required model is assembled/created.
    //Note in the case of request, the passed id parameter is ignored.
    public Product getFacilityModel(String model, Long id, String type, int capacity, double price, double maintenance, String equipment){
        if(model.toUpperCase().equals("REQUEST")){
            return new FacilityRequest(type,capacity, price, maintenance, equipment);
        }
        if(model.toUpperCase().equals("RESPONSE")){
            return new FacilityResponse(id, type,capacity, price, maintenance, equipment);
        }
        if(model.toUpperCase().equals("ENTITY")){
            return new FacilityEntity(id, type,capacity, price, maintenance, equipment);
        }

        return new Facility(id,type,capacity,price,maintenance,equipment);
    }
}
