package com.SmokeySoft.ResourceManagement.Controllers;

import com.SmokeySoft.ResourceManagement.Models.Facility;
import com.SmokeySoft.ResourceManagement.Models.FacilityRequest;
import com.SmokeySoft.ResourceManagement.Models.FacilityResponse;
import com.SmokeySoft.ResourceManagement.Services.FacilityService;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
public class FacilityController{

   @Autowired
    FacilityService facilityService;

   @Autowired
    ModelMapper modelMapper;

   @PostMapping("/facility") //matching the endpoints
   @ResponseStatus(HttpStatus.CREATED)
   public //indicating created status
   FacilityResponse newFacility(@Valid @RequestBody FacilityRequest facilityRequest){

       Facility facility = modelMapper.map(facilityRequest, Facility.class);
       //mapping the request into a facility

       Facility savedFacility = facilityService.saveFacility(facility);
       //saving the facility through the service layer

       FacilityResponse facilityResponse = modelMapper.map(savedFacility, FacilityResponse.class);
       //mapping the savedfacility into a response and returning it
       return facilityResponse;
   }

    @GetMapping("/facility/{id}") //matching the endpoints for get request
    @ResponseStatus(HttpStatus.FOUND) //indicating found status
    @ResponseBody //writes data to the body of the response object
    public
    ResponseEntity<FacilityResponse> findOne(@PathVariable Long id) {

       Facility foundFac = facilityService.getFacilityById(id);
       //Function call to retrieve the id through the service layer

        if(foundFac == null){
             return new ResponseEntity<FacilityResponse> (HttpStatus.NOT_FOUND);
             //we return a null, and not found response entity
        }

       FacilityResponse returnedResponse = modelMapper.map(foundFac, FacilityResponse.class);
       //function returns a response entity, so we to facility response and convert it to a response entity
       return  new ResponseEntity<FacilityResponse> (returnedResponse, HttpStatus.FOUND);

   }

    @GetMapping("/facility/")
    @ResponseStatus(HttpStatus.FOUND)
    @ResponseBody
    ResponseEntity<List<FacilityResponse>>findAll(){

       List<Facility> foundFacilities = facilityService.getAllFacilities();
       //retrieving the facilities through the service layer

       if(foundFacilities == null){ //if the list is empty, then we output the appropriate status code
           return new ResponseEntity<List<FacilityResponse>> (HttpStatus.NOT_FOUND);
       }

       List<FacilityResponse> facilityResponses = new ArrayList<FacilityResponse>(); // list to store responses

       for(int i=0; i<foundFacilities.size(); i++){ //each facility, is converted to a response and added to the list
           FacilityResponse addToResponseList = modelMapper.map(foundFacilities.get(i), FacilityResponse.class);
           facilityResponses.add(addToResponseList);
        }

        return new ResponseEntity<List<FacilityResponse>> (facilityResponses, HttpStatus.FOUND);
       //returning the response entity as a list of facility responses
    }

    @DeleteMapping("/facility/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    ResponseEntity<FacilityResponse> DeleteFacilityById(@PathVariable Long id){

        boolean retStatus = facilityService.deleteFacilityById(id);
        //deleting by ID through the service layer

        if(!retStatus){ //If we don't find the required message we return as seen below
            System.out.println("Please make sure this entity exists");
            return new ResponseEntity<FacilityResponse> (HttpStatus.BAD_REQUEST);
        }

        //If it is found we return the response entity as seen below
        return new ResponseEntity<FacilityResponse> (HttpStatus.NO_CONTENT);
    }

    @PutMapping("/facility/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<FacilityResponse> saveOrUpdate(@RequestBody FacilityRequest facilityRequest, @PathVariable Long id) {

      //We map the request to a facility, and update the facility through the service layer
      Facility facilityToBeUpdated = modelMapper.map(facilityRequest, Facility.class);
      Facility updatedFacility = facilityService.updateFacility(id, facilityToBeUpdated);

      if(updatedFacility == null){ //if we do not find the facility we indicate through the status code
          return new ResponseEntity<FacilityResponse> (HttpStatus.NOT_FOUND);
      }

      FacilityResponse updatedResponse = modelMapper.map(updatedFacility, FacilityResponse.class);
      //we map back to a response, and return as a response Entity

      return new ResponseEntity<FacilityResponse> (updatedResponse, HttpStatus.OK);
    }


}
