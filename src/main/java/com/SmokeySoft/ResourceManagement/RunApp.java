package com.SmokeySoft.ResourceManagement;


import com.SmokeySoft.ResourceManagement.Models.Facility;
import com.SmokeySoft.ResourceManagement.Models.FacilityRequest;
import com.SmokeySoft.ResourceManagement.Models.FacilityResponse;
import com.SmokeySoft.ResourceManagement.Models.ProductFactory;
import com.SmokeySoft.Timetabling.BookingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.context.ApplicationContext;
import java.util.Scanner;


@SpringBootApplication
public class RunApp {

    @Autowired
    private ApplicationContext context;

    public static void main(String[] args) {

        ApplicationContext Resources = SpringApplication.run(RunApp.class, args);
        Menu();
        SpringApplication.exit(Resources);

    }

    //Menu Function
    public static void Menu() {

        Scanner read = new Scanner(System.in); //creating a scanner instance
        int userOption;

        RestTemplate restTemplate = new RestTemplate(); //rest template for RESTful API communication

        String endpoint = "http://localhost:8080/facility"; //endpoint to this API
        String timetableEndpoint = "http://localhost:8082/bookings"; //endpoint to timetabling API

        ProductFactory factory = new ProductFactory(); //creating  a factory to create models

        do {

            System.out.println("****Resource Management Main Menu*****"); //Menu for API
            System.out.println("\n\n1.Create a new Facility");
            System.out.println("2. Find a Facility By id");
            System.out.println("3. Find all Facilities");
            System.out.println("4. Delete Facility by Id");
            System.out.println("5. Update a Facility");
            System.out.println("6. Queries");
            System.out.println("7. Exit");
            System.out.println("----------------------------------------");
            System.out.println("\nPlease choose an option");
            userOption = read.nextInt();
            read.nextLine(); //buffer clear



            switch (userOption) { //switch case based on user's choice

                case 1:
                    //Retrieving user input
                    System.out.println("\nPlease enter the type of facility. (Board Room, Office Space, Hot Desk)");
                    String type = read.nextLine();
                    System.out.println("Enter the capacity");
                    int capacity = read.nextInt();
                    read.nextLine();
                    System.out.println("Enter the price");
                    double price = read.nextDouble();
                    read.nextLine();
                    System.out.println("Enter the maintenance");
                    double maintenance = read.nextDouble();
                    read.nextLine();
                    System.out.println("Enter the equipment. (Format: xxx/xxx/xxx)");
                    String equipment = read.nextLine();

                    System.out.println("\n\n");

                    try {
                        //creating Htpp Entity request. Facility Request created through the factory
                        HttpEntity<FacilityRequest> request = new HttpEntity<FacilityRequest>((FacilityRequest) factory.getFacilityModel("request",0L,type,capacity, price, maintenance,equipment));
                        ResponseEntity<String> create = restTemplate.postForEntity(endpoint, request, String.class);
                        //Posting the entity (i.e creation of entity)

                        if(create.getStatusCode() == HttpStatus.CREATED){ //Handling of error codes
                            System.out.println("\nFacility has been created\n");
                        }

                    }catch(HttpClientErrorException e){ //Exception handling
                        if(e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                            System.out.println("\nPlease respect the constraints. Facility not created\n");
                        }else{
                            System.out.println("\nSome error has occurred\n");
                        }
                        break;
                    }

                    break;
                case 2:
                    //Asking user for details
                    System.out.println("Enter the id of the facility you want to find. (i.e 1)");
                    Long id = read.nextLong();
                    read.nextLine();

                    try { //Attempting the fetching of the entity
                        ResponseEntity<String> response = restTemplate.getForEntity(endpoint + "/" + id, String.class);

                        if (response.getStatusCode() == HttpStatus.FOUND) { //Handling of codes
                            System.out.println(response.getBody());
                        }

                    }catch(HttpClientErrorException e){//Error handling
                        if(e.getStatusCode() == HttpStatus.NOT_FOUND) {
                            System.out.println("\nPlease Enter Id which exists\n");
                        }else{
                            System.out.println("\nSome error has occurred\n");
                        }
                    }

                    break;
                case 3:

                    try{//Attempting to retrieve the all the entities
                        ResponseEntity<FacilityResponse[]> responseEntity = restTemplate.getForEntity(endpoint + "/", FacilityResponse[].class);

                        if(responseEntity.getStatusCode() == HttpStatus.FOUND) { // Making sure the error code is as we expect
                            System.out.println("\nFind all facilities below:\n");

                            FacilityResponse[] allFacilities = responseEntity.getBody(); //Storing the bodies in the array

                            for (int i = 0; i < allFacilities.length; i++) { //Displaying all the details for each facility
                                allFacilities[i].FacilityDisplay();
                            }
                        }

                    }catch(HttpClientErrorException e){//Error handling
                        if(e.getStatusCode() == HttpStatus.NOT_FOUND){
                            System.out.println("There seems to be no facilities in the system");
                        }else{
                            System.out.println("Some error has occurred");
                        }
                    }
                    break;
                case 4:

                    System.out.println("Enter the id of the facility you wish to delete: ");
                    Long idToDelete = read.nextLong(); //Asking for details from user
                    read.nextLine();

                    try {
                        //Creating request
                        HttpEntity<Long> idRequest = new HttpEntity<Long>(idToDelete);
                        ResponseEntity<FacilityResponse> returnedResponse = restTemplate.exchange(endpoint + "/" + idToDelete, HttpMethod.DELETE, idRequest, FacilityResponse.class);
                        //performing the delete

                        if (returnedResponse.getStatusCode() == HttpStatus.NO_CONTENT) { // making sure appropriate response was received
                            System.out.println("\nFacility with id " + idToDelete + " has been deleted");
                        }

                        //The below code depends on the timetabling microservice
                        //Note : for this to work timetabling microservice should be set to port 8082

                        //Retrieving all bookings
                        ResponseEntity<BookingResponse[]> responseEntity =
                                restTemplate.getForEntity(timetableEndpoint + "/", BookingResponse[].class);

                        if(responseEntity.getStatusCode() == HttpStatus.FOUND) {
                            BookingResponse[] allBkgs = responseEntity.getBody();
                            //Once found we store in an array

                            for (int i = 0; i < allBkgs.length; i++) {
                                if (allBkgs[i].getFacilityNo() == idToDelete) { //find the booking under the facility ID

                                    //Delete the booking from the system
                                    HttpEntity<Long> idDeleteRequestBooking = new HttpEntity<Long>(allBkgs[i].getSId());
                                    restTemplate.exchange(timetableEndpoint + "/" + allBkgs[i].getSId(), HttpMethod.DELETE, idDeleteRequestBooking, BookingResponse.class);
                                }

                            }
                        }

                    }catch(HttpClientErrorException e){ //error handling
                        if(e.getStatusCode() == HttpStatus.BAD_REQUEST){
                            System.out.println("Facility not in system");
                        }else if(e.getStatusCode() == HttpStatus.NOT_FOUND){
                            System.out.println("Not associated with any booking");
                        }else{
                            System.out.println("Error occurred. Unable to delete");
                        }
                    }
                    break;
                case 5:
                    //Requesting details for update
                    System.out.println("Please enter the id of the facility that you wish to be replaced");
                    Long idToUpdate = read.nextLong();
                    read.nextLine();

                    System.out.println("Enter the details of the new facility");
                    System.out.println("\nPlease enter the type of facility. (Board Room, Office Space, Hot Desk)");
                    String typeUpdate = read.nextLine();
                    System.out.println("Enter the capacity");
                    int capacityUpdate = read.nextInt();
                    read.nextLine();
                    System.out.println("Enter the price");
                    double priceUpdate = read.nextDouble();
                    read.nextLine();
                    System.out.println("Enter the maintenance");
                    double maintenanceUpdate = read.nextDouble();
                    read.nextLine();
                    System.out.println("Enter the equipment. (Format: xxx/xxx/xxx)");
                    String equipmentUpdate = read.nextLine();

                    System.out.println("\n\n");

                    try{
                        //preparing the request, and changing it to HTTP entity
                        //Since this is an object instantiation we may use the factory. Note any long may be passed for id.
                        FacilityRequest request = (FacilityRequest) factory.getFacilityModel("request", 0L,typeUpdate,capacityUpdate, priceUpdate, maintenanceUpdate, equipmentUpdate);
                        HttpEntity<FacilityRequest> httpRequest = new HttpEntity<>(request);

                        ResponseEntity<FacilityResponse> returnedResponse = restTemplate.exchange(endpoint + "/" +idToUpdate, HttpMethod.PUT, httpRequest, FacilityResponse.class);
                        //performing the update

                        if(returnedResponse.getStatusCode() == HttpStatus.OK ){//Handling of codes
                            System.out.println("The facility has been updated");
                        }


                    }catch(HttpClientErrorException e){//Error handling
                        if(e.getStatusCode() == HttpStatus.NOT_FOUND){
                            System.out.println("This facility was unable to updated");
                        }else{
                            System.out.println("Some error occurred");
                        }
                    }
                    break;

                case 6:

                    System.out.println("Would you like to query by: "); //Query options
                    System.out.println("1. Minimum capacity");
                    System.out.println("2. Maximum price");
                    System.out.println("3. Maximum maintenance");
                    System.out.print("Enter no: ");
                    int choice = read.nextInt();
                    read.nextLine();



                    try {//Attempting to retrieve the all the entities
                        ResponseEntity<FacilityResponse[]> responseEntity = restTemplate.getForEntity(endpoint + "/", FacilityResponse[].class);

                        if (responseEntity.getStatusCode() == HttpStatus.FOUND) { // Making sure the error code is as we expect
                            System.out.println("\n");

                            FacilityResponse[] allFacilities = responseEntity.getBody(); //Storing the bodies in the array

                            if(choice == 1){ //If's based on choice

                                System.out.println("Enter the capacity"); //Retrieving details
                                int qCap = read.nextInt();
                                read.nextLine();

                                for (int i = 0; i < allFacilities.length; i++) { //Displaying all the details for each facility
                                    if(allFacilities[i].getCapacity() >= qCap) { //which satisfies the query
                                        allFacilities[i].FacilityDisplay();
                                    }
                                }

                            }else if(choice == 2){
                                System.out.println("Enter the price"); //Same thing for price
                                double qPrice = read.nextDouble();
                                read.nextLine();

                                for (int i = 0; i < allFacilities.length; i++) { //Displaying all the details for each facility
                                    if(allFacilities[i].getPrice() <= qPrice) { //which satisfies the query
                                        allFacilities[i].FacilityDisplay();
                                    }
                                }

                            }else if(choice == 3){
                                System.out.println("Enter the maintenance"); //Same thing for maintenance
                                double qMaintenance = read.nextDouble();
                                read.nextLine();

                                for (int i = 0; i < allFacilities.length; i++) { //Displaying all the details for each facility
                                    if(allFacilities[i].getMaintenance() <= qMaintenance) { //which satisfy the queries
                                        allFacilities[i].FacilityDisplay();
                                    }
                                }

                            }else{ //for invalid option choice
                                System.out.println("Invalid option");
                            }
                            break;
                        }
                    }catch (HttpClientErrorException | NullPointerException e){ //Error handling
                        System.out.println("Unable to query");
                    }

                case 7:
                    System.exit(0); //Shutting down system
                    break;
                default:
                    System.out.println("Please choose a valid option"); //Handle invalid options
            }

        }while(userOption != 7); //repeat until exit
    }
}


