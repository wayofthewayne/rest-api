package com.SmokeySoft.ResourceManagement.Repository;

import com.SmokeySoft.ResourceManagement.Models.FacilityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FacilityRepo extends JpaRepository<FacilityEntity, Long>{


}
