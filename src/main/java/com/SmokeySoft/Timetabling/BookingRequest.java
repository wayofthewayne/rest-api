package com.SmokeySoft.Timetabling;

//Request and Response classes for establishing communication

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;



public class BookingRequest {

    @NotBlank(message = "ID number required")
    @Pattern(regexp = "[0-9]{3,7}[LMGAHlmgah]")//id number should adhere to the Maltese national Id number standard.
    private String id;

    @NotBlank(message = "Company name required")
    private String companyName;

    private String startDate;
    private String endDate;
    private int facilityNo;
    public BookingRequest() {
        this(null, null,null,null,0);
    }

    public BookingRequest(String companyName, String id, String startDate, String endDate, int facilityNo) {
        this.setCompanyName(companyName);
        this.setId(id);
        this.setStartDate(startDate);
        this.setEndDate(endDate);
        this.setFacilityNo(facilityNo);
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate(){return endDate;}


    public void setFacilityNo(int facilityNo){
        this.facilityNo = facilityNo; // facility.facilityNo
    }

    public int getFacilityNo(){return facilityNo;}
    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public void setEndDate(String endDate){
        this.endDate = endDate;
    }


    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getCompanyName() {
        return companyName;
    }

    public String getId(){
        return this.id;
    }
    public void setId(String id){this.id = id;}

}
