package com.SmokeySoft.Timetabling;

public class BookingResponse extends BookingRequest {
    private Long sId;

    public BookingResponse() {
        this(null, null, null, null, null, 0);
    }

    public BookingResponse(Long sId,  String companyName, String id,String startDate ,String endDate, int facilityNo) {
        super( companyName,id, startDate, endDate, facilityNo);
        this.setSId(sId);
    }


    public Long getSId() {
        return sId;
    }

    public void setSId(Long id) {
        this.sId = id;
    }


}
